function openSearch() {
	var search = $(".search-right");
	$('.dropdown-menu').hide();
	$('.burger-menu').addClass("collapsed");
	$(".collapse.show").removeClass("show");
	if(search.css("display")=="none") {
		$(".search-mobile").addClass("active");
		search.slideDown();
	}
	else {
		$(".search-mobile").removeClass("active");
		search.slideUp();
	}
}
$(document).ready(function() {
	/* owl carousel */
	$('.owl-carousel-featured').owlCarousel({
		items: 3,
		loop: true,
		mouseDrag: false,
		nav: true,
		dotsEach: true,
		navText: ['<img src=\'img/icons/left-arrow.png\'>', '<img src=\'img/icons/right-arrow.png\'>'],
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			578: {
				items: 2
			},
			991: {
				items: 3
			}
		}
	});
	$('.owl-full').owlCarousel({
		items: 4,
		loop: true,
		mouseDrag: false,
		nav: true,
		dotsEach: true,
		navText: ['<img src=\'img/icons/left-arrow.png\'>', '<img src=\'img/icons/right-arrow.png\'>'],
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			578: {
				items: 2
			},
			768: {
				items: 3
			},
			991: {
				items: 4
			}
		}
	});

	$('.mini-header .dropdown').on('show.bs.dropdown', function() {
		$('.burger-menu').addClass("collapsed");
		$(".collapse.show").removeClass("show");
		$(".search-right").hide();
		$(".search-mobile").removeClass("active");
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});

	$('.mega-menu .dropdown').on('show.bs.dropdown', function() {
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	});


	$('.mega-menu .collapse').on('show.bs.collapse', function() {
		$(".search-right").hide();
		$(".search-mobile").removeClass("active");
	});

	$('.dropdown').on('hide.bs.dropdown', function() {
	    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	    $(".search-right").hide();
	    $(".search-mobile").removeClass("active");
	});
});